package Bean;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import java.io.*;
import java.net.URL;
import java.time.LocalDateTime;

public class PupuGoods {
    private static String urlStr;   //网站地址
    private static JSONObject jsonStr;  //json格式String类型字符串
    private static JSONObject data; //朴朴头部key值
    private static String name;     //商品名称
    private static double goodsPrice;   //商品价格

    /**
     * 主函数
     * @param args
     */
    public static void main(String[] args) {
        head(); //调用head方法
    }

    /**
     * 将获取到的网站信息由String类型转化成Json类型的字符串
     * 将获取到的字符串进行分类，得到商品名称、价格……
     * 输出信息
     */
    public static void head() {
        //将得到的网站数据从String类型转化成Json类型的字符串
        jsonStr = (JSONObject) JSONSerializer.toJSON(getGoodsPUPU());
        //获取朴朴data KEY键值
        data = jsonStr.getJSONObject("data");
        name = data.getString("name");     //获取商品名字信息
        String goodsSpec = data.getString("spec");    //获取商品规格
        goodsPrice = data.getDouble("price");  //获取商品价格
        double goodsPriceTwo = data.getDouble("market_price");    //获取商品原价
        String goodsSub = data.getString("sub_title");    //获取商品详细内容

        System.out.println("---------商品：" + name + "---------");
        System.out.println("规格：" + goodsSpec);
        System.out.println("价格:" + goodsPrice / 100 + "元");
        System.out.println("原价:" + goodsPriceTwo / 100 + "元");
        System.out.println("详细内容:" + goodsSub);
        System.out.println(""); //


        System.out.println("-------------" + name + "的价格波动-------------");
        try {
            //设置计时器，每五秒更新一次商品价格信息
            timer(name, goodsPrice);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 调用getURLContent方法，获取网站内的内容
     *
     * @return 网站内的String类型字符串内容
     */
    public static String getGoodsPUPU() {
        try {
            //抓取网站地址
            urlStr = "https://j1.pupuapi.com/client/product/storeproduct/detail/deef1dd8-65ee-46bc-9e18-8cf1478a67e9/bbca8f38-351a-435b-8262-6544c4b96041";
        } catch (JsonIOException e) {
            e.printStackTrace();
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
        return getURLContent(urlStr);
    }

    /**
     * IO流操作，将获取网站信息，并将网站中的数据转化为String类型字符串
     *
     * @param urlStr 参数为网站地址
     * @return 字符串类型的网站信息
     */
    public static String getURLContent(String urlStr) {
        URL url = null;     //路径对象
        BufferedReader in = null;
        StringBuilder sb = new StringBuilder();
        try {
            url = new URL(urlStr);  //将网站地址赋值给路径对象

            //输入流能够读取、访问网站上的数据信息，并将信息设置格式为utf-8
            in = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"));
            String str = null;
            //将网站的信息输入到字符串中
            while ((str = in.readLine()) != null) {
                sb.append(str);
            }
        } catch (Exception ex) {
            ex.printStackTrace();

            //最后都要判断网站内是否有信息，没有则释放内存
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        //把StringBuilder类型的字符串数据输入到String类型字符串中
        String result = sb.toString();
        return result;
    }


    /**
     * 计时器，每五秒进入一次网页，获取最新的商品名称和价格
     *
     * @param name
     * @param price
     * @throws IOException
     * @throws InterruptedException
     */
    public static void timer(String name, double price) throws IOException, InterruptedException {
        //每5秒抓取一次商品信息
        while (true) {
            getURLContent(urlStr);  //每一次都获取一次网站内的信息

            System.out.println("当前时间为" + LocalDateTime.now() + " ," + name + ":价格为" + price / 100);
            Thread.sleep(5000);
        }
    }
}
